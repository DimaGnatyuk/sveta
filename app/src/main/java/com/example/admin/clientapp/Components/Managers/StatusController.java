package com.example.admin.clientapp.Components.Managers;

import com.example.admin.clientapp.Components.Data.StatusItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 04.06.2016.
 */
public class StatusController {
    private static List<StatusItem> statusItems;

    public StatusController(JSONObject rootObject) throws JSONException {
        List<StatusItem> statusItems = new ArrayList<>();

        JSONArray jsonArray =  rootObject.getJSONArray("data");
        for (int i=0;i<jsonArray.length();i++)
        {
            JSONObject info = jsonArray.getJSONObject(i);
            JSONObject statysCollection = info.getJSONObject("StatysCollection");
            StatusItem statusItem = new StatusItem(statysCollection.getInt("id"),statysCollection.getString("name"));
            statusItems.add(statusItem);
        }

        this.statusItems = statusItems;
    }

    public void update (List<StatusItem> statusItems){
        this.statusItems = statusItems;
    }

    public List<StatusItem> getStatusItems (){
        return statusItems;
    }

    public StatusItem getStatusItem (int id){
        return statusItems.get(id);
    }
}
