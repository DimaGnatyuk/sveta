package com.example.admin.clientapp.Components.Data;

/**
 * Created by Admin on 04.06.2016.
 */
public class StatusItem {
    private int id;
    private String name;

    public StatusItem(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
