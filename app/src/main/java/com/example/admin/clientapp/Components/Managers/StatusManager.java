package com.example.admin.clientapp.Components.Managers;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Admin on 04.06.2016.
 */
public class StatusManager {
    private static StatusController statusController = null;

    public static StatusController getIntent(JSONObject jsonObject) throws JSONException {
        if (statusController == null){
            statusController = new StatusController(jsonObject);
        }
        return statusController;
    }
}
