package com.example.admin.clientapp.Components.Data;

/**
 * Created by Admin on 04.06.2016.
 */
public class StatusItemUser {
    private int id;
    private int idStatus;
    private String data;
    private boolean isChack;



    public StatusItemUser(int idStatus) {
        this.idStatus = idStatus;
    }
    public StatusItemUser(int id, boolean isChack) {
        this.id = id;
        this.isChack = isChack;
    }


    public StatusItemUser(int id, int idStatus, String data, boolean isChack) {
        this.id = id;
        this.idStatus = idStatus;
        this.data = data;
        this.isChack = isChack;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdStatus() {
        return idStatus;
    }

    public void setIdStatus(int idStatus) {
        this.idStatus = idStatus;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public boolean isChack() {
        return isChack;
    }

    public void setIsChack(boolean isChack) {
        this.isChack = isChack;
    }
}
