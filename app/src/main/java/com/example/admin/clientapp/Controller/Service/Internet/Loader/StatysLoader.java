package com.example.admin.clientapp.Controller.Service.Internet.Loader;

import android.os.AsyncTask;

import com.example.admin.clientapp.Components.ConfigURL;
import com.example.admin.clientapp.Components.Data.StatusItemUser;
import com.example.admin.clientapp.Controller.Service.Internet.Loader.Interface.LoaderResultStatus;

import org.json.JSONException;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Response;

/**
 * Created by Admin on 04.06.2016.
 */
public class StatysLoader  extends AsyncTask<String, Void, Response> {

    private int idStatus = -1;
    private StatusItemUser statusItemUser = null;
    private LoaderResultStatus mLoaderResult;
    private boolean update = false;
    private String token;
    private String role;

    public StatysLoader(StatusItemUser statusItemUser,LoaderResultStatus mLoaderResult, boolean update,String token,String role) {
        this.statusItemUser = statusItemUser;
        this.mLoaderResult = mLoaderResult;
        this.update = update;
        this.token = token;
        this.role=role;
    }

    public StatysLoader(int idStatus, LoaderResultStatus mLoaderResult,String token,String role) {
        this.idStatus = idStatus;
        this.mLoaderResult = mLoaderResult;
        this.token = token;
        this.role=role;
    }

    public StatysLoader(LoaderResultStatus mLoaderResult,String token,String role) {
        this.mLoaderResult = mLoaderResult;
        this.token = token;
        this.role=role;
    }


    @Override
    protected Response doInBackground(String... params) {
        Response response = null;
        OkHttpClient client = new OkHttpClient();
        try {
            if (idStatus != -1){
                response = client.newCall(ConfigURL.findStatysById(token, idStatus,role)).execute();
            }else if (statusItemUser != null){
                if (this.update){
                    response = client.newCall(ConfigURL.verefiStatys(token, statusItemUser.getId(),role,statusItemUser.isChack())).execute();
                }else{
                    response = client.newCall(ConfigURL.addStatys(token, statusItemUser.getIdStatus(),role)).execute();
                }
            }else {
                response = client.newCall(ConfigURL.getAllStatys(token)).execute();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    @Override
    protected void onPostExecute(Response response) {
        super.onPostExecute(response);
        if (idStatus != -1){
            try {
                mLoaderResult.LoaderResultById(response);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else if (statusItemUser != null){
            try {
                mLoaderResult.LoaderResultAddedOrUpdate(response);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else {
            mLoaderResult.LoaderResultAll(response);
        }
    }
}
