package com.example.admin.clientapp.Controller;

import android.content.SharedPreferences;

/**
 * Created by Admin on 03.06.2016.
 */
public class RoleManager {
    private static RoleController roleController = null;

    public static RoleController getIntents (SharedPreferences sharedPreferences){
        if (roleController == null){
            roleController = new RoleController(sharedPreferences);
        }
        return roleController;
    }

}
