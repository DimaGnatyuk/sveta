package com.example.admin.clientapp.Controller;

import android.content.SharedPreferences;

import com.example.admin.clientapp.Components.DataConfig;

/**
 * Created by Admin on 03.06.2016.
 */
public class RoleController {

    private static String thisRole = null;
    private static String thisToken = null;
    private static String thisUser = null;

    public static SharedPreferences sharedPreferences;

    public RoleController(SharedPreferences sharedPreferences){
        this.sharedPreferences = sharedPreferences;
        thisUser = sharedPreferences.getString(DataConfig.USER,null);
        thisToken = sharedPreferences.getString(DataConfig.USER_TOKEN,null);
        thisRole = sharedPreferences.getString(DataConfig.USER_ROLE,null);
    }

    public void login (String name, String token, String role){
        thisRole = role;
        thisToken = token;
        thisUser = name;
        update ();
    }

    public void logout (){
        sharedPreferences.edit().clear().commit();
        thisUser = null;
        thisToken = null;
        thisRole = null;
    }

    public boolean userRole (String role){
        return thisUser == role;
    }

    public boolean validation (){
        boolean status = true;
        if (thisUser == null || thisToken == null || thisRole == null){
            status = false;
        }
        return status;
    }

    public boolean update (){
        boolean status = validation();
        if (status) {
            sharedPreferences.edit()
                    .putString(DataConfig.USER, thisUser)
                    .putString(DataConfig.USER_TOKEN, thisToken)
                    .putString(DataConfig.USER_ROLE, thisRole)
                    .commit();
        }
        return status;
    }

    public String getUser (){
        return thisUser == null?"":thisUser;
    }

    public String getToken (){
        return thisToken == null?"":thisToken;
    }

    public String getRole (){
        return thisRole == null?"":thisRole;
    }
}
