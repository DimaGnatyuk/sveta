package com.example.admin.clientapp.Controller.Service.Internet.Loader.Interface;

import org.json.JSONException;

import java.io.IOException;

import okhttp3.Response;

/**
 * Created by Admin on 04.06.2016.
 */
public interface LoaderResultStatus {
    void LoaderResultAll (Response response);
    void LoaderResultById (Response response) throws IOException, JSONException;
    void LoaderResultAddedOrUpdate (Response response) throws IOException, JSONException;
}
