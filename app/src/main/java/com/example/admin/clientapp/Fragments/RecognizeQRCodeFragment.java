package com.example.admin.clientapp.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.google.zxing.integration.android.IntentIntegrator;

/**
 * Created by Admin on 03.06.2016.
 */
public class RecognizeQRCodeFragment extends Fragment {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new IntentIntegrator(this.getActivity()).setOrientationLocked(true).setCameraId(0).setPrompt("I Scanini...").initiateScan();
    }
}
