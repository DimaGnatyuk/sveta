package com.example.admin.clientapp.Components;

/**
 * Created by Admin on 03.06.2016.
 */
public class DataConfig {
    public static final String APP_SP = "APP_SP";

    public static final String ROLE_USER = "_USER";
    public static final String ROLE_ADMIN = "_ADMIN";

    public static final String USER = "_user";
    public static final String USER_ROLE = "_role";
    public static final String USER_TOKEN = "_token";

    public static final String ADMIN_EMAIL = "admin@ukr.net";
    public static final String ADMIN_PASSWORD = "123456";
}
