package com.example.admin.clientapp;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.admin.clientapp.Components.Data.StatusItemUser;
import com.example.admin.clientapp.Components.DataConfig;
import com.example.admin.clientapp.Components.Managers.StatusController;
import com.example.admin.clientapp.Components.Parsers.Parse;
import com.example.admin.clientapp.Controller.RoleController;
import com.example.admin.clientapp.Controller.RoleManager;
import com.example.admin.clientapp.Controller.Service.Internet.Loader.Interface.LoaderResultStatus;
import com.example.admin.clientapp.Controller.Service.Internet.Loader.StatysLoader;
import com.example.admin.clientapp.Fragments.GenerationQRCodeFragment;
import com.example.admin.clientapp.Fragments.ListStatusFragment;
import com.example.admin.clientapp.Fragments.RecognizeQRCodeFragment;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Response;

public class MainActivity extends AppCompatActivity implements LoaderResultStatus {

    private static SharedPreferences sharedPreferences;
    private static RoleController roleController;
    private static FragmentManager fragmentManager;
    private static FragmentTransaction fragmentTransaction;
    private static StatusController statusController;
    private LoaderResultStatus loaderResultStatus;
    private AlertDialog.Builder ad;
    private Context context;
    private int idHistoryStatys;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        try {
            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            this.sharedPreferences = getSharedPreferences(DataConfig.APP_SP, MODE_PRIVATE);
            this.roleController = RoleManager.getIntents(sharedPreferences);
            this.fragmentManager = getSupportFragmentManager();
            loaderResultStatus = (LoaderResultStatus) this;
            this.context = this;

            ad = new AlertDialog.Builder(this);
            ad.setTitle("Title");  // заголовок
            ad.setMessage("message"); // сообщение
            ad.setPositiveButton("Підтвердження", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int arg1) {
                    Toast.makeText(context, "Заявку підтвердженно",
                            Toast.LENGTH_LONG).show();
                    StatusItemUser statusItemUser = new StatusItemUser(idHistoryStatys, true);
                    new StatysLoader(statusItemUser, loaderResultStatus, true, roleController.getToken(), roleController.getRole()).execute();
                }
            });
            ad.setNegativeButton("Відхилення", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int arg1) {
                    Toast.makeText(context, "Заявку відхилено", Toast.LENGTH_LONG)
                            .show();
                    StatusItemUser statusItemUser = new StatusItemUser(idHistoryStatys, false);
                    new StatysLoader(statusItemUser, loaderResultStatus, true, roleController.getToken(), roleController.getRole()).execute();
                }
            });
            ad.setCancelable(true);
            ad.setOnCancelListener(new DialogInterface.OnCancelListener() {
                public void onCancel(DialogInterface dialog) {
                    Toast.makeText(context, "Вы ничего не выбрали",
                            Toast.LENGTH_LONG).show();
                }
            });

            final FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);

            fab.setEnabled(false);
            fab.setVisibility(View.GONE);
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        if (roleController.getRole().compareToIgnoreCase(DataConfig.ROLE_ADMIN) == 0) {
                            fragmentTransaction = fragmentManager.beginTransaction();
                            RecognizeQRCodeFragment fragment = new RecognizeQRCodeFragment();
                            fragmentTransaction.add(R.id.frameLayout, fragment);
                            fragmentTransaction.commit();
                        } else {
                            fragmentTransaction = fragmentManager.beginTransaction();
                            ListStatusFragment listStatusFragment = new ListStatusFragment(statusController.getStatusItems(), roleController);
                            fragmentTransaction.replace(R.id.frameLayout, listStatusFragment);
                            fragmentTransaction.commit();
                        }
                    } catch (Exception e) {

                    }
                }
            });
            if (!this.roleController.validation()) {
                startActivity(new Intent(this, LoginActivity.class));
                finish();
            } else {
                if (this.roleController.getRole().compareToIgnoreCase(DataConfig.ROLE_ADMIN) == 0) {
                    fab.setEnabled(true);
                    fab.setVisibility(View.VISIBLE);
                    fragmentTransaction = fragmentManager.beginTransaction();
                    RecognizeQRCodeFragment fragment = new RecognizeQRCodeFragment();
                    fragmentTransaction.add(R.id.frameLayout, fragment);
                    fragmentTransaction.commit();
                } else if (this.roleController.getRole().compareToIgnoreCase(DataConfig.ROLE_USER) == 0) {
                    fab.setEnabled(true);
                    fab.setVisibility(View.VISIBLE);
                    new StatysLoader(this, roleController.getToken(), roleController.getRole()).execute();
                } else {
                    this.roleController.logout();
                    startActivity(new Intent(this, MainActivity.class));
                    finish();
                }
            }
        }catch (Exception error){
            startActivity(new Intent(this, LoginActivity.class));
            finish();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
            if (scanningResult != null) {
                if (scanningResult.getContents().compareToIgnoreCase("") == 0) {
                    Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show();
                } else {
                    String scanContent = scanningResult.getContents();
                    int id = Integer.decode(scanContent);
                    Toast.makeText(this, scanContent, Toast.LENGTH_SHORT).show();
                    new StatysLoader(id, loaderResultStatus, roleController.getToken(), roleController.getRole()).execute();
                }
            }
        }catch (Exception error){

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_logout) {
            this.roleController.logout();
            startActivity(new Intent(this,MainActivity.class));
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void LoaderResultAll(Response response) {
        String s = null;
        try {
            s = response.body().string();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            JSONObject jsonObject = new JSONObject(s);
            statusController = Parse.parseStatusItem(jsonObject);
            fragmentTransaction = fragmentManager.beginTransaction();
            ListStatusFragment listStatusFragment = new ListStatusFragment(statusController.getStatusItems(),roleController);
            fragmentTransaction.replace(R.id.frameLayout, listStatusFragment);
            fragmentTransaction.commit();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void LoaderResultById(Response response) throws IOException, JSONException {
        String s = response.body().string();
        JSONObject jsonObject = new JSONObject(s);
        JSONObject data = jsonObject.getJSONObject("data");
        JSONObject user = data.getJSONObject("User");
        JSONObject historyStatys = data.getJSONObject("HistoryStatys");
        JSONObject statysCollection = data.getJSONObject("StatysCollection");
        String userName =  user.getString("name");
        String userEmail = user.getString("email");

        idHistoryStatys =  historyStatys.getInt("id");
        String dataHistoryStatys =  historyStatys.getString("data");

        String statysCollectionName = statysCollection.getString("name");

        String message = "Data: "+dataHistoryStatys+"\n"+
                "User"+userName+"|"+userEmail+"\n"+
                "Status - "+statysCollectionName;
        ad.setMessage(message);
        ad.show();
    }

    @Override
    public void LoaderResultAddedOrUpdate(Response response) throws IOException, JSONException {
        String s = response.body().string();
        if (roleController.getRole().compareToIgnoreCase(DataConfig.ROLE_USER) == 0){
            JSONObject rootObject = new JSONObject(s);
            StatusItemUser statusItemUser = Parse.parseStatusItemUser(rootObject);

            fragmentTransaction = fragmentManager.beginTransaction();
            GenerationQRCodeFragment fragment = new GenerationQRCodeFragment(statusItemUser);
            fragmentTransaction.replace(R.id.frameLayout, fragment);
            fragmentTransaction.commit();
        }else{

        }
    }
}
