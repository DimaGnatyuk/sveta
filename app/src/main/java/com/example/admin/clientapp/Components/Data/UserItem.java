package com.example.admin.clientapp.Components.Data;

/**
 * Created by Admin on 04.06.2016.
 */
public class UserItem {
    private int id;
    private String name;
    private String email;
    private String token;

    public UserItem() {

    }

    public UserItem(int id, String name, String email, String token) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.token = token;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getToken() {
        return token;
    }
}
