package com.example.admin.clientapp.Components;

import okhttp3.FormBody;
import okhttp3.Request;
import okhttp3.RequestBody;

/**
 * Created by Admin on 31.05.2016.
 */
public class ConfigURL {
    private static String URL_ROOT = "http://android-projects.esy.es/";
    private static String API_ROOT = "api/";
    private static String BASE_URL = URL_ROOT+API_ROOT;

    public static Request auntification (String email, String password){
        RequestBody formBody = new FormBody.Builder()
                .add("email", email)
                .add("password", password)
                .build();

        Request request = new Request.Builder()
                .url(BASE_URL + "login")
                .post(formBody)
                .build();
        return request;
    }

    public static Request getAllStatys (String token){
        Request request = new Request.Builder()
                .url(BASE_URL + "getAllStatys?key=" + token)
                .build();
        return request;
    }

    public static Request findStatysById (String token, int idStatys,String role){
        RequestBody formBody = new FormBody.Builder()
                .add("role", role)
                .build();

        Request request = new Request.Builder()
                .url(BASE_URL + "findStatysById/"+idStatys+"/?key="+token)
                .post(formBody)
                .build();
        return request;
    }

    public static Request addStatys (String token, int idStatys,String role){
        RequestBody formBody = new FormBody.Builder()
                .add("role", role)
                .add("idStatys", String.valueOf(idStatys))
                .build();

        Request request = new Request.Builder()
                .url(BASE_URL + "addStatys/?key="+token)
                .post(formBody)
                .build();
        return request;
    }

    public static Request verefiStatys (String token, int idStatys,String role,boolean check){
        RequestBody formBody = new FormBody.Builder()
                .add("role", role)
                .add("check", String.valueOf(check?1:0))
                .build();

        Request request = new Request.Builder()
                .url(BASE_URL + "verefiStatys/"+idStatys+"/?key="+token)
                .post(formBody)
                .build();
        return request;
    }
}
