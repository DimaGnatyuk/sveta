package com.example.admin.clientapp.Components.Parsers;

import android.util.Log;

import com.example.admin.clientapp.Components.Data.StatusItemUser;
import com.example.admin.clientapp.Components.Data.UserItem;
import com.example.admin.clientapp.Components.Managers.StatusController;
import com.example.admin.clientapp.Components.Managers.StatusManager;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Admin on 04.06.2016.
 */
public class Parse {

    public static UserItem parseUserItem (JSONObject rootObject) throws JSONException {
        UserItem user = null;
        JSONObject object =  rootObject.getJSONObject("data");
        Log.d("JSON", object.toString());
        JSONObject peopleInfo = object.getJSONObject("User");
        user = new UserItem(peopleInfo.getInt("id"),peopleInfo.getString("name"),peopleInfo.getString("email"),peopleInfo.getString("has_key"));
        return user;
    }

    public static StatusController parseStatusItem (JSONObject object) throws JSONException {
        StatusController statusManager = StatusManager.getIntent(object);
        return statusManager;
    }

    public static StatusItemUser parseStatusItemUser (JSONObject rootObject) throws JSONException {
        StatusItemUser statusItemUser = null;
        JSONObject object =  rootObject.getJSONObject("data");
        Log.d("JSON", object.toString());
        JSONObject info = object.getJSONObject("HistoryStatys");
        statusItemUser = new StatusItemUser(info.getInt("id"),info.getInt("statys_collection_id"),info.getString("data"),false);
        return statusItemUser;
    }

    public static boolean test (JSONObject object) throws JSONException {
        return  object.getInt("error") == 0 ? true : false;
    }

    public static String parseError(JSONObject jsonObject) throws JSONException {
        return jsonObject.getString("data");
    }
}
