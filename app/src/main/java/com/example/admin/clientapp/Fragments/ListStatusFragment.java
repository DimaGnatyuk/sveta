package com.example.admin.clientapp.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.admin.clientapp.Components.Data.StatusItem;
import com.example.admin.clientapp.Components.Data.StatusItemUser;
import com.example.admin.clientapp.Controller.RoleController;
import com.example.admin.clientapp.Controller.Service.Internet.Loader.Interface.LoaderResultStatus;
import com.example.admin.clientapp.Controller.Service.Internet.Loader.StatysLoader;
import com.example.admin.clientapp.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 04.06.2016.
 */
public class ListStatusFragment extends Fragment {

    private FragmentManager fragmentManager;
    private FragmentTransaction fragmentTransaction;
    private List<StatusItem> statusItems= null;
    private ListView listView;
    private FloatingActionButton fab;
    private RoleController roleController;

    public ListStatusFragment (List<StatusItem> statusItems,RoleController roleController){
        this.statusItems = statusItems;
        this.roleController = roleController;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        fragmentManager = getActivity().getSupportFragmentManager();
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.list_status_fragment, container, false);
    }

    @Override
    public void onStart() {
        listView = (ListView) this.getActivity().findViewById(R.id.list_status);
        fab = (FloatingActionButton) this.getActivity().findViewById(R.id.fab);

        List<String> stringList = new ArrayList<>();
        for (StatusItem statusItem:statusItems) {
            stringList.add(statusItem.getName());
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this.getContext(), android.R.layout.simple_list_item_1, stringList);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(view.getContext(), statusItems.get(position).getName(), Toast.LENGTH_SHORT).show();
                fab.setEnabled(true);
                StatusItemUser statusItemUser = new StatusItemUser(statusItems.get(position).getId());
                new StatysLoader(statusItemUser,(LoaderResultStatus)getActivity(),false,roleController.getToken(),roleController.getRole()).execute();
            }
        });
        super.onStart();
    }
}