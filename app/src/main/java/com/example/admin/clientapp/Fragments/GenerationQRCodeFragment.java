package com.example.admin.clientapp.Fragments;

import android.graphics.Bitmap;
import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;

import com.example.admin.clientapp.Components.Data.StatusItemUser;
import com.example.admin.clientapp.Components.QRClasses.Contents;
import com.example.admin.clientapp.Components.QRClasses.QRCodeEncoder;
import com.example.admin.clientapp.R;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;

/**
 * Created by Admin on 03.06.2016.
 */
public class GenerationQRCodeFragment extends Fragment {

    private ImageView imageView;
    private StatusItemUser statusItemUser;

    public GenerationQRCodeFragment (StatusItemUser statusItemUser){
        this.statusItemUser = statusItemUser;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_generation_qr_code, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        imageView = (ImageView)this.getActivity().findViewById(R.id.img_qr_code);
        recognizeQrCode();
    }

    private void recognizeQrCode (){
        WindowManager manager = (WindowManager) this.getActivity().getSystemService(this.getActivity().WINDOW_SERVICE);
        Display display = manager.getDefaultDisplay();
        Point point = new Point();
        display.getSize(point);
        int width = point.x;
        int height = point.y;
        int smallerDimension = width < height ? width : height;
        smallerDimension = smallerDimension * 3/4;

        QRCodeEncoder qrCodeEncoder = new QRCodeEncoder(String.valueOf(statusItemUser.getId()),
                null,
                Contents.Type.TEXT,
                BarcodeFormat.QR_CODE.toString(),
                smallerDimension);
        try {
            Bitmap bitmap = qrCodeEncoder.encodeAsBitmap();
            imageView.setImageBitmap(bitmap);

        } catch (WriterException e) {
            e.printStackTrace();
        }
    }
}
