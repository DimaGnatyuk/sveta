package com.example.admin.clientapp.Controller.Service.Internet.Loader;


import android.os.AsyncTask;

import com.example.admin.clientapp.Components.ConfigURL;
import com.example.admin.clientapp.Controller.Service.Internet.Loader.Interface.LoaderResult;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Response;

/**
 * Created by Admin on 04.06.2016.
 */
public class AuntificationLoader extends AsyncTask<String, Void, Response> {

    private final String mEmail;
    private final String mPassword;
    private LoaderResult mLoaderResult;

    public AuntificationLoader(String email, String password, LoaderResult loaderResult) {
        mEmail = email;
        mPassword = password;
        mLoaderResult = loaderResult;
    }


    @Override
    protected Response doInBackground(String... params) {
        Response response = null;
        OkHttpClient client = new OkHttpClient();
        try {
            response = client.newCall(ConfigURL.auntification(mEmail, mPassword)).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    @Override
    protected void onPostExecute(Response response) {
        super.onPostExecute(response);
        mLoaderResult.LoaderResult(response);
    }
}
