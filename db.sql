-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               5.5.25 - MySQL Community Server (GPL)
-- ОС Сервера:                   Win32
-- HeidiSQL Версия:              9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Дамп структуры базы данных sveta_db
CREATE DATABASE IF NOT EXISTS `sveta_db` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `sveta_db`;


-- Дамп структуры для таблица sveta_db.history_statys
CREATE TABLE IF NOT EXISTS `history_statys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `statys_collection_id` int(11) NOT NULL,
  `data` date NOT NULL,
  `isCheck` tinyint(1) DEFAULT NULL,
  `dateUpdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id_statys_collection_id_data` (`user_id`,`statys_collection_id`,`data`,`id`,`dateUpdate`),
  KEY `FK_history_statys_statys_collections` (`statys_collection_id`),
  CONSTRAINT `FK_history_statys_statys_collections` FOREIGN KEY (`statys_collection_id`) REFERENCES `statys_collections` (`id`),
  CONSTRAINT `FK_history_statys_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы sveta_db.history_statys: ~6 rows (приблизительно)
DELETE FROM `history_statys`;
/*!40000 ALTER TABLE `history_statys` DISABLE KEYS */;
INSERT INTO `history_statys` (`id`, `user_id`, `statys_collection_id`, `data`, `isCheck`, `dateUpdate`) VALUES
	(6, 5, 1, '0000-00-00', NULL, '2016-06-04 00:59:26'),
	(17, 5, 1, '2016-06-04', NULL, '2016-06-04 01:05:42'),
	(19, 5, 1, '2016-06-04', NULL, '2016-06-04 01:08:17'),
	(20, 5, 1, '2016-06-04', NULL, '2016-06-04 01:08:19'),
	(21, 5, 1, '2016-06-04', NULL, '2016-06-04 01:08:21'),
	(22, 5, 1, '2016-06-04', 1, '2016-06-04 01:08:23');
/*!40000 ALTER TABLE `history_statys` ENABLE KEYS */;


-- Дамп структуры для таблица sveta_db.statys_collections
CREATE TABLE IF NOT EXISTS `statys_collections` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы sveta_db.statys_collections: ~6 rows (приблизительно)
DELETE FROM `statys_collections`;
/*!40000 ALTER TABLE `statys_collections` DISABLE KEYS */;
INSERT INTO `statys_collections` (`id`, `name`) VALUES
	(3, 'Відкритий лікарняний'),
	(4, 'Закритий лікарняний'),
	(5, 'Звільнений'),
	(6, 'Прийнятий'),
	(1, 'Прихід'),
	(2, 'Ухід');
/*!40000 ALTER TABLE `statys_collections` ENABLE KEYS */;


-- Дамп структуры для таблица sveta_db.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `email` varchar(50) NOT NULL,
  `has_key` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `has_key` (`has_key`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы sveta_db.users: ~3 rows (приблизительно)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `email`, `has_key`) VALUES
	(5, 'ret', 'ertert@ukr.net', '1d073477749b3d85293865e02c549a67'),
	(6, 'dgf', 'dfg@ukr.net', '20c02dda0f402520837df5070556ac2f'),
	(9, 'test', 'test@ukr.net', '70dba953fc2d0fce88bcfe73b485ae67');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
